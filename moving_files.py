# 12.4 Challenge: Move All Image Files To a New Directory
# In the chapter 12 practice_files folder, there is a subfolder called
# documents/. The directory contains several files and subfolders. Some
# of the files are images ending with the .png, .gif, or .jpg file extension.
# Create a new folder in the practice_files folder called images/, and
# move all image files to that folder.

from pathlib import Path

documents = Path.home() / "practice_files" / "documents"

images_folder = Path.home() / "images"
images_folder.mkdir(exist_ok=True)

suffixes = [".png", ".jpg", ".gif"]

for path in documents.rglob("*.*"):
    if path.suffix.lower() in suffixes:
        path.replace(images_folder / path.name)