# Write a program called factors.py that asks the user to input a positive integer 
# and then prints out the factors of that number.


num = int(input("Enter a positive integer:\n"))
for n in range(1,num+1):
    if num % n == 0:
        print(f"{str(n)} is a factor of {str(num)}.")
    else:
        continue