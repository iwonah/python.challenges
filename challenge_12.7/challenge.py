from pathlib import Path
import csv

scores_path = Path.cwd() / "scores.csv"

with scores_path.open(mode="r", encoding="utf-8", newline="") as file:
    high_scores = {}
    reader = csv.DictReader(file)
    for player in reader:
        name = player['name']
        score = int(player['score'])

        if name in high_scores:
            if score > high_scores[name]:
                high_scores[name] = score
        else:
            high_scores[name] = score

high_scores_path = Path.cwd() / "high_scores.csv"
with high_scores_path.open(mode="w", encoding="utf-8") as file:
    writer = csv.DictWriter(file, fieldnames=["name", "high_score"])
    writer.writeheader()
    for name in high_scores:
        row_dict = {"name": name, "high_score": high_scores[name]}
        writer.writerow(row_dict)