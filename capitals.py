# Pick a random state name from the dictionary and assign both
# the state and its capital to two variables. You’ll need to import the
# random module at the top of your program.
# Then display the name of the state to the user and ask them to enter
# the capital. If the user answers incorrectly, then repeatedly ask them
# for the capital until they either enter the correct answer or type "exit".
# If the user answers correctly, then display "Correct" 
# and end the program. However, if the user exits without guessing correctly, display
# the correct answer and the word "Goodbye".
# Make sure the user isn’t punished for case sensitivity.

import random

capitals_dict = {
    "Alabama": "Montgomery",
    "Alaska": "Juneau",
    "Arizona": "Phoenix",
    "Arkansas": "Little Rock",
    "California": "Sacramento",
    "Colorado": "Denver",
    "Connecticut": "Hartford",
    "Delaware": "Dover",
    "Florida": "Tallahassee",
    "Georgia": "Atlanta",
    "Hawaii": "Honolulu",
    "Idaho": "Boise",
    "Illinois": "Springfield",
    "Indiana": "Indianapolis",
    "Iowa": "Des Moines",
    "Kansas": "Topeka",
    "Kentucky": "Frankfort",
    "Louisiana": "Baton Rouge",
    "Maine": "Augusta",
    "Maryland": "Annapolis",
    "Massachusetts": "Boston",
    "Michigan": "Lansing",
    "Minnesota": "Saint Paul",
    "Mississippi": "Jackson",
    "Missouri": "Jefferson City",
    "Montana": "Helena",
    "Nebraska": "Lincoln",
    "Nevada": "Carson City",
    "New Hampshire": "Concord",
    "New Jersey": "Trenton",
    "New Mexico": "Santa Fe",
    "New York": "Albany",
    "North Carolina": "Raleigh",
    "North Dakota": "Bismarck",
    "Ohio": "Columbus",
    "Oklahoma": "Oklahoma City",
    "Oregon": "Salem",
    "Pennsylvania": "Harrisburg",
    "Rhode Island": "Providence",
    "South Carolina": "Columbia",
    "South Dakota": "Pierre",
    "Tennessee": "Nashville",
    "Texas": "Austin",
    "Utah": "Salt Lake City",
    "Vermont": "Montpelier",
    "Virginia": "Richmond",
    "Washington": "Olympia",
    "West Virginia": "Charleston",
    "Wisconsin": "Madison",
    "Wyoming": "Cheyenne",
}

state, capital = random.choice(list(capitals_dict.items()))

print(state,capital)

answer = input(f"What is the capital of {state}? (To exit the program, enter 'exit'):\n")

while True:
    if answer.lower() != capital.lower() and answer.lower() != "exit" :
        print(f"{answer} is not the capital of {state}.")
        answer = input(f"Try again. What is the capital of {state}?(To exit the program, enter 'exit'):\n")
    elif answer.lower() == "exit":
        print("Goodbye")
        break
    else:
        print(f"Your answer is correct, {answer} is the capitol of {state}!")
        break