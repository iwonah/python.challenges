def get_cats_with_hats(array_of_cats):
    cats_with_hats_on = []
    for num in range(1,101):
        for cat in range(1,101):
            if cat % num == 0:
                if array_of_cats[cat] == "hat":
                    array_of_cats[cat] = "no hat"
                else:
                    array_of_cats[cat] = "hat"

    for cat in range(1,101):
        if array_of_cats[cat] == "hat":
            cats_with_hats_on.append(f"Cat no {str(cat)} has a hat")

    return cats_with_hats_on

cats = ["no hat" for num in range(101)]
print(get_cats_with_hats(cats))