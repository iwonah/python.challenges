# With some help from the random module and a little conditional logic,
# you can simulate an election between two candidates.
# Suppose two candidates, Candidate A and Candidate B, are running
# for mayor in a city with three voting regions. The most recent polls
# show that Candidate A has the following chances for winning in each region:
# • Region 1: 87 percent chance of winning
# • Region 2: 65 percent chance of winning
# • Region 3: 17 percent chance of winning
# Write a program that simulates the election ten thousand times and
# prints the percentage of times in which Candidate A wins.
# To keep things simple, assume that a candidate wins the election if
# they win in at least two of the three regions

from random import random

def election_regional(chance_of_winning):
    if random() < chance_of_winning:
        return "a"
    else:
        return "b"

def general_eection():
    outcome = []
    if election_regional(0.87) == "a":
        outcome.append("a")
    else:
        outcome.append("b")

    if election_regional(0.65) == "a":
        outcome.append("a")
    else:
        outcome.append("b")

    if election_regional(0.17) == "a":
        outcome.append("a")
    else:
        outcome.append("b")

    if outcome.count("a") > outcome.count("b"):
        return "a"
    else:
        return "b"
    
def election(num_of_simulations):
    a_wins = 0
    b_wins = 0
    for simulation in range(num_of_simulations):
        if general_eection() == "a":
            a_wins += 1
        else:
            b_wins +=1
    print(f"Probability of candidate A winning equals: {a_wins / num_of_simulations}.")
    print(f"Probability of candidate B winning equals: {b_wins / num_of_simulations}.")
          
election(10_000)
