# Write a program called temperature.py that defines two functions:
# 1. convert_cel_to_far()
# 2. convert_far_to_cel()
# The program should do the following:
# 1. Prompt the user to enter a temperature in degrees Fahrenheit and
# then display the temperature converted to Celsius
# 2. Prompt the user to enter a temperature in degrees Celsius and then
# display the temperature converted to Fahrenheit
# 3. Display all converted temperatures rounded to two decimal places

def convert_far_to_cel():
    degrees_far = float(input("Enter a temperature in degrees Fahrenheit:\n"))
    degrees_cel = (degrees_far -32) * 5/9
    print(f"{degrees_far} degrees F = {degrees_cel:.2f} degrees C")

def convert_cel_to_far():
    degrees_cel = float(input("Enter a temperature in degrees Celsius:\n"))
    degrees_far = degrees_cel * 9/5 + 32
    print(f"{degrees_cel} degrees F = {degrees_far:.2f} degrees C")

convert_far_to_cel()
convert_cel_to_far()
