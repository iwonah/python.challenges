# Write a program that will print out the amount of the investment,
# rounded to two decimal places, at the end of each year for the
# specified number of years.

def invest(amount, rate, years):
    """Display year on year growth of an initial investment"""
    for year in range(1, years+1):
        amount = amount * (1 + rate)
        print(f"{year} year:${amount:.2f}")

amount = float(input("Enter the amount you are going to invest:\n"))
rate = float(input("Enter the rate of your investment:\n"))
years = int(input("Enter the number of years youre going to invest for:\n"))

invest(amount, rate, years)