import random
words = ["Dumbledore", "Muggle", "Quiddich", "Poltergeist"]

def guess():
    guess = input("Guess a letter:\n")
    while True:
        if len(guess) == 1 and guess.isalpha():
            return guess.upper()
        else:
            print("That was not a valid guess. Try again:")
            guess = input()
    
def print_hangman(number_of_mistakes):
    if number_of_mistakes == 0:
        print("""
            +---+
                |
                |
                |
               ===
               """)
    elif number_of_mistakes == 1:
        print("""
            +---+
            O   |
                |
                |
               ===
            """)
    elif number_of_mistakes == 2:
        print("""
            +---+
            O   |
            |   |
                |
               ===
            """)
    elif number_of_mistakes == 3:
        print("""
            +---+
            O   |
           /|   |
                |
               ===
            """)
    elif number_of_mistakes == 4:
        print("""
            +---+
            O   |
           /|\  |
                |
               ===
            """)
    elif number_of_mistakes == 5:
        print("""
            +---+
            O   |
           /|\  |
           /    |
               ===
            """)
    elif number_of_mistakes == 6:
        print("""
            +---+
            O   |
           /|\  |
           / \  |
               ===
            """)

def greetings():
    print("THE HANGMAN")
    name = input("What's your name?\n")
    print(f"Hi, {name}. Welcome to the Hangman game!")
    print("Enter letters to guess a word, you are allowed 5 errors.")

def game_on():
    greetings()
    word_to_guess = random.choice(words).upper()
    letters_guessed = ["_"] * len(word_to_guess)
    number_of_mistakes = 0

    while word_to_guess != "".join(letters_guessed) and number_of_mistakes < 6:

        for underscore in letters_guessed:
            print(underscore, end ="")
        print()
        g = guess()
        
        if g in word_to_guess and g not in letters_guessed:
            indices = [i for i, char in enumerate(word_to_guess) if char == g]
            for i in indices:
                letters_guessed[i] = g
        else:
            number_of_mistakes += 1

        print_hangman(number_of_mistakes)

    if word_to_guess == "".join(letters_guessed):
        print(f"{word_to_guess} \nCongratulations, the word to guess was {word_to_guess}.")
        print("You won!")
    else:
        print_hangman(6)
        print("You let him die, shame on you.")
        print("Game Over.")

game_on()
