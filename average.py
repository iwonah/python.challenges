# Suppose you flip a fair coin repeatedly until it lands on heads and tails
# at least one time each. In other words, after the first flip, you continue
# to flip the coin until it lands on the other side.
# Doing this generates a sequence of heads and tails. For example, the
# first time you do this experiment, the sequence might be heads, heads,
# tails.
# On average, how many flips are needed for the sequence to contain
# both heads and tails?
# Write a simulation that runs ten thousand trials of the experiment and
# prints the average number of flips per trial.

from random import randint

def flip (): return randint(0,1)

def single_sequence():
    heads = 0
    tails = 0
    while heads == 0 or tails == 0:
        flip()
        if flip() == 0:
            heads += 1
        else:
            tails += 1
    flips_num = heads + tails
    return flips_num

def average_flips(number_of_trials):
    sum_of_flips = 0
    for i in range(number_of_trials):
        sum_of_flips += single_sequence()
    average = sum_of_flips / number_of_trials
    return average

print(average_flips(10_000))