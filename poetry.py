import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango",
"extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows",
"explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant",
"exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon",
"for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly",
"furiously", "sensuously"]

random_nouns = {random.choice(nouns) for num in range(3)}
while len(random_nouns) != 3:
    random_nouns.add(random.choice(nouns))
noun1, noun2, noun3 = random_nouns

random_verbs = {random.choice(verbs) for num in range(3)}
while len(random_verbs) != 3:
    random_verbs.add(random.choice(verbs))
ver1, ver2, ver3 = random_verbs

random_adj = {random.choice(adjectives) for num in range(3)}
while len(random_adj) != 3:
    random_adj.add(random.choice(adjectives))
adj1, adj2, adj3 = random_adj

random_prep = {random.choice(prepositions) for num in range(2)}
while len(random_prep) != 2:
    random_prep.add(random.choice(prepositions))
prep1, prep2 = random_prep

adv = random.choice(adverbs)

if adj1[0] in "aeiou":
    article = "An"
else: 
    article = "A"

poem = (
    f"{article} {adj1} {noun1}\n\n"
    f"{article} {adj1} {noun1} {ver1} {prep1} the {adj2} {noun2}\n"
    f"{adv}, the {noun1} {ver2}\n"
    f"the {noun2} {ver3} {prep2} a {adj3} {noun3}\n"
)

print(poem)

