# Challenge: Model a Farm
# Create a simplified model of a farm.
# The actual requirements are open to interpretation, but try to adhere
# to these guidelines:
# 1. You should have at least four classes: the parent
# at least three child animal classes that inherit from
# Animal class and Animal.
# 2. Each class should have a few attributes and at least one method
# that models some behavior appropriate for a specific animal or all
# animals—walking, running, eating, sleeping, and so on.
# 3. Keep it simple. Utilize inheritance. Make sure you output details
# about the animals and their behaviors.

class Animal:
    money = 5
    food = 5

    def __init__(self, name):
        self.name = name

    def speak(self, sound):
        return f"{self.name} says {sound}!"
    
    def pet(self):
        return f"{self.name} is content and much happier now!"
    
    def feed_animal(self):
        if self.nutrition_intake == 0 and self.food > 0:
            self.food -= 1
            self.nutrition_intake += 1
            print(f"{self.name}: *Chewing sounds* I'm not hungry anymore, thanks!")
        elif self.nutrition_intake > 0:
            print(f"{self.name}: I'm not hungry now, eat that yourself.")
        else:
            print("Seems like you don't have enough food. Go on and buy some.")

    def buy_food(self):
        if self.money > 0:
            self.money -= 1
            self.food += 3
            print(f"You've just bought some food for your animals. Current amount of food:{self.food}")
        elif self.money == 0:
            print(f"You cannot buy anyting, earn some money first.")

class Pig(Animal):
    nutrition_intake = 0

    def speak(self, sound = "Oink, oink"):
        return super().speak(sound)

class Hen(Animal):
    eggs = 1
    nutrition_intake = 0

    def sell_eggs(self):
        if self.eggs == 0:
            print("Nothing to sell, feed your chickens first!")
        else:
            self.eggs -= 1
            Animal.money += 1
            print(f"One egg less, one coin more. Good job. Current amount of money: {Animal.money}.")

    def lay_egg(self):
        if self.nutrition_intake == 1:
            self.eggs += 1
            self.nutrition_intake -= 1
            print(f"{self.name} has just laid an egg. Current amount of eggs: {self.eggs}.")
        elif self.nutrition_intake == 0:
            print(f"{self.name}: I'm hungry, you won't get any eggs from me.")

    def speak(self, sound = "ko, ko, ko"):
        return super().speak(sound)

class Cow(Animal):
    nutrition_intake = 0
    milk = 0

    def get_milk(self):
        if self.nutrition_intake == 1:
            self.milk += 1
            self.nutrition_intake -= 1
            print(f"You've just milked {self.name}. Current liters of milk: {self.milk}.")
        elif self.nutrition_intake == 0:
            print(f"{self.name}: I'm hungry, you won't get any milk from me.")

    def sell_milk(self, amount):
        if self.milk == 0 or amount > self.milk:
            print("Nothing to sell, feed your cow first!")
        else:
            self.milk -= amount
            Animal.money += 3 * amount
            print(f"You've just succesfully sold {amount} liters of milk. Current amount of money: {Animal.money}.")

    def speak(self, sound = "moooooo"):
        return super().speak(sound)

hen1 = Hen("Clara")
pig1 = Pig("Peppa")
cow1 = Cow("Molly")

print(hen1.speak())
print(hen1.pet())
hen1.sell_eggs()
print(f"The current amount of money is {Animal.money}.")
hen1.feed_animal()
hen1.lay_egg()
hen1.buy_food()
pig1.feed_animal()
cow1.get_milk()
cow1.feed_animal()
