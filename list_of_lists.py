# Define a function, enrollment_stats(), with a single parameter. This
# parameter should be a list of lists in which each individual list contains
# three elements:
# 1. The name of a university
# 2. The total number of enrolled students
# 3. The annual tuition fees
# enrollment_stats() should return two lists, the first containing all the
# student enrollment values and the second containing all the tuition
# fees.
# Next, define two functions, mean() and median(), that take a single list
# argument and return the mean or median of the values in each list,
# respectively.
# Using universities, enrollment_stats(), mean(), and median(), calculate
# the total number of students, the total tuition, the mean and median
# numbers of students, and the mean and median tuition values.
# 25


universities = [
['California Institute of Technology', 2175, 37704],
['Harvard', 19627, 39849],
['Massachusetts Institute of Technology', 10566, 40732],
['Princeton', 7802, 37000],
['Rice', 5879, 35551],
['Stanford', 19535, 40569],
['Yale', 11701, 40500]
]

def enrollment_stats(list_of_lists):
    numbers_of_students = [element[1] for element in list_of_lists]
    tuition_fees = [element[2] for element in list_of_lists]
    return [numbers_of_students, tuition_fees]

def mean(a_list):
    return (sum(a_list) / len(a_list))

def median(a_list):
    a_list.sort()
    return a_list[round((len(a_list) - 1) / 2)]

number_of_students, tuition_fees = enrollment_stats(universities)
mean_num_of_students = round(mean(number_of_students))
median_num_of_students = round(median(number_of_students))
mean_tuition_value = round(mean(tuition_fees),2)
median_tuition_fee = round(median(tuition_fees),2)

print("******************************")
print(f"Total students: {sum(number_of_students)}")
print(f"Total tuition: $ {sum(tuition_fees)}\n")
print(f"Student mean: {mean_num_of_students}")
print(f"Student median: {median_num_of_students}\n")
print(f"Tuition mean: $ {mean_tuition_value}")
print(f"Tuition median: $ {median_tuition_fee}")
print("******************************")