class School:

    def __init__(self, first_name, last_name, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def __str__(self):
        return f"{self.first_name} {self.last_name} is {self.age} years old."

class Employees(School):

    def __init__(self, first_name, last_name, age, salary):
        super().__init__(first_name, last_name, age)
        self.salary = salary
        self.email = f"{first_name}{last_name}@school.com"

    def __str__(self):
        return f"{self.first_name} {self.last_name} is {self.age} years old. Contact: {self.email}"

    def do_extra_hours():
        pass

class Teachers(Employees):

    def __init__(self, first_name, last_name, age, salary, subject):
        super().__init__(first_name, last_name, age, salary)
        self.subject = subject

    def make_an_exam():
        pass

    def check_attendence():
        pass

    def teach(self):
        print(f"{self.first_name} {self.last_name} is teaching {self.subject}")

class SupervisingTeachers(Teachers):
    
    def __init__(self, first_name, last_name, age, salary, subject, class_supervised):
        super().__init__(first_name, last_name, age, salary, subject)
        self.class_supervised = class_supervised

class Administration(Employees):
    
    def hire_an_empl():
        pass

    def dismiss_an_empl():
        pass

    def give_raise():
        pass

class Student(School):

    subjects = {"Subject1" : [], "Subject2" : []}
    warnings = 0
    
    def __init__(self, first_name, last_name, age, class_):
        super().__init__(first_name, last_name, age)
        self.class_ = class_

    def add_grades(subject, grade):
        pass

    def add_warning():
        pass

    def join_a_SIG():
        pass

    def display_grades():
        pass

teacher1 = Teachers("John", "White", 35, 30000, "Math")
print(teacher1.teach())
print(teacher1)

student1 = Student("Anna", "Black", 8, 2)
print(student1)